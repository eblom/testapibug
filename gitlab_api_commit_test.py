import json
import requests
import random
import sys

token = sys.argv[1]

CI_API_V4_URL = "https://gitlab.com/api/v4"
CI_PROJECT_ID = "21002227"
branch = "master"
actions = [
    {'action': 'update',
     'file_path': '/test.file',
     'content': random.randint(1,100)}
]

headers = {
    "PRIVATE-TOKEN": token,
    "Content-Type": "application/json",
}

payload = {
    "branch": branch,
    "commit_message": "Test pipeline via API calls",
    "actions": actions,
    "author_name": "Eblom",
    "author_email": "example@example.com",
}

data = json.dumps(payload)
url = f"{CI_API_V4_URL}/projects/{CI_PROJECT_ID}/repository/commits"
response = requests.post(
    url,
    headers=headers,
    data=data,
)

print(response)

if response.ok:
    print("Sucessfully committed.")
else:
    raise RuntimeError("Committing to branch failed.")
